from cottonwood.core.layers.dense import Dense
from cottonwood.core.layers.difference import Difference
from cottonwood.core.layers.range_normalization import RangeNormalization
from cottonwood.core.model import ANN
import cottonwood.data.data_loader_nordic_runes as dat


training_set, evaluation_set = dat.get_data_sets()
sample = next(training_set)
n_pixels = sample.shape[0] * sample.shape[1]

layers = []
layers.append(RangeNormalization(training_set))
layers.append(Dense(17, previous_layer=layers[-1]))
layers.append(Dense(13, previous_layer=layers[-1]))
layers.append(Dense(8, previous_layer=layers[-1]))
layers.append(Dense(12, previous_layer=layers[-1]))
layers.append(Dense(19, previous_layer=layers[-1]))
layers.append(Dense(n_pixels, previous_layer=layers[-1]))
layers.append(Difference(layers[-1], layers[0]))

autoencoder = ANN(layers=layers)
autoencoder.train(training_set)
autoencoder.evaluate(evaluation_set)
