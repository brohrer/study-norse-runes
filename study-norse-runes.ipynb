{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Neural networks are weirdly good at translating languages\n",
    "and identifying dogs by breed, but they can be intimidating\n",
    "to get started with. In an effort to smooth this on-ramp,\n",
    "I created a neural network framework specifically for\n",
    "teaching and experimentation. It’s called\n",
    "[Cottonwood](https://gitlab.com/brohrer/cottonwood)\n",
    "and this notebook shows how to build a first network.\n",
    "\n",
    "# What you’ll get out of this case study\n",
    "\n",
    "You will get to build an honest-to-goodness neural network,\n",
    "a densely connected 8-layer autoencoder and run it on an\n",
    "image dataset. This will set you up well for subsequent projects\n",
    "using your framework of choice, whether it's Cottonwood, PyTorch,\n",
    "Tensorflow."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# What you'll need to get started\n",
    "\n",
    "To run this notebook, you'll need\n",
    "[Python 3](https://www.python.org/downloads/),\n",
    "[Jupyter](https://jupyter.org/install),\n",
    "[Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git), and access to your command line.\n",
    "\n",
    "No prior machine learning experience necessary."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Get cottonwood\n",
    "\n",
    "To get Cottonwood, navigate your command line to directory you'd like it to live in.\n",
    "Then clone the Git repository (it's less than 1 MB), and install the Cottonwood package.\n",
    "The \"-e\" option lets you edit your copy of Cottonwood on the fly, opening up\n",
    "all kinds of opportunities for experimentation.\n",
    "\n",
    "```bash\n",
    "git clone https://gitlab.com/brohrer/cottonwood.git\n",
    "python3 -m pip install -e cottonwood\n",
    "```\n",
    "\n",
    "It's also important to specify the version of Cottonwood we want to use.\n",
    "Because Cottonwood young and designed to encourage experimentation, it's not\n",
    "guaranteed to be backward compatible. The version matters. This case study runs well on version 14.\n",
    "\n",
    "```bash\n",
    "cd cottonwood\n",
    "git checkout v14\n",
    "```\n",
    "\n",
    "When you're done, navigate to where you want this notebook to live and open a new one up there. Alternatively, you can clone this repository and use this notebook directly.\n",
    "\n",
    "```bash\n",
    "git clone https://gitlab.com/brohrer/study-norse-runes.git\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Import some packages\n",
    "\n",
    "The first thing to do is to pull in the Cottonwood building blocks we’re going to need. These include several types of layers, a `Dense` layer, a `Difference` layer, and a `RangeNormalization` layer. It also includes an `ANN` (artificial neural network) model and a Nordic runes data set that comes prepackaged with Cottonwood for coding up examples."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cottonwood.core.layers.dense import Dense\n",
    "from cottonwood.core.layers.difference import Difference\n",
    "from cottonwood.core.layers.range_normalization import RangeNormalization\n",
    "from cottonwood.core.model import ANN\n",
    "import cottonwood.data.data_loader_nordic_runes as dat"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "# Get some data\n",
    "\n",
    "Then we need to pull the data in. The `get_data_sets()` function in our data loader pulls in two generators, a `training_set` and an `evaluation_set`. A generator is a convenient Python object that hands you a new example every time you call `next()` on it. We put this to the test by calling `next(training_set)` and getting a first example of our data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "training_set, evaluation_set = dat.get_data_sets()\n",
    "sample = next(training_set)\n",
    "n_pixels = sample.shape[0] * sample.shape[1]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can take a quick look at it and see that it's a two dimensional array of zeros and ones, a very crude representation of an image. It has seven rows and seven columns, 49 pixels it all. If you squint and tilt your head you can just make out the pattern. This data set is a collection of 24 Norse runes, coarsely pixelized."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(sample)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Create some layers\n",
    "\n",
    "Now we get to construct the neural network itself. The first step is to add a `RangeNormalization` layer. This ensures that the data falls in a range that can be meaningfully interpreted by the network.\n",
    "\n",
    "Then we have a sequence of six `Dense` layers. The first argument is the number of outputs that the layer will have. The second argument explicitly connects each `Dense` layer to the previous layer. The `Dense` layer uses the previous layer to know how many inputs it should expect.\n",
    "\n",
    "Here we have five hidden layers and an output layer. The hidden layer with the smallest number of nodes is the bottleneck layer. At this layer the information contained in the images is at its highest compression. Instead of 49 separate pixel values, it’s reduced down to eight node activities.\n",
    "\n",
    "Finally, we add a `Difference` layer to find how close the output of the autoencoder is to the input. This gives us an error signal that we can try to make as small as possible, re-creating the outputs with as much fidelity as we can."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "layers = []\n",
    "layers.append(RangeNormalization(training_set))\n",
    "layers.append(Dense(17, previous_layer=layers[-1]))\n",
    "layers.append(Dense(13, previous_layer=layers[-1]))\n",
    "layers.append(Dense(8, previous_layer=layers[-1]))\n",
    "layers.append(Dense(12, previous_layer=layers[-1]))\n",
    "layers.append(Dense(19, previous_layer=layers[-1]))\n",
    "layers.append(Dense(n_pixels, previous_layer=layers[-1]))\n",
    "layers.append(Difference(layers[-1], layers[0]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "# Build and run the model\n",
    "\n",
    "Finally it’s time to fire up the autoencoder! We pass in the layers to the `ANN` model to initialize it. Then, thanks to all of our preparatory work, training the model just requires calling its `train()` method and passing at the `training_data_set`. Similarly evaluating the model is as straightforward as calling its `evaluate()` method and passing the `evaluation_data_set`.\n",
    "\n",
    "When the model is done running, it returns its error history. If the model behavior is behaving well, the error gets smaller over time."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "autoencoder = ANN(layers=layers)\n",
    "autoencoder.train(training_set)\n",
    "autoencoder.evaluate(evaluation_set)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When the model runs, it also saves out some informative reports. In the `reports` directory, we can inspect `model_parameters.txt`. This human readable text file gives all the information necessary to re-create this exact model in a different framework.\n",
    "\n",
    "```text\n",
    "type: artificial neural network\n",
    "number of training iterations: 800000\n",
    "number of evaluation iterations: 200000\n",
    "error_function:  mean squared error\n",
    "layer 0:  range normalization\n",
    "  range maximum: 1\n",
    "  range minimum: 0\n",
    "layer 1:  fully connected\n",
    "  number of inputs: 49\n",
    "  number of outputs: 17\n",
    "  activation function:  hyperbolic tangent\n",
    "  initialization:  LSUV\n",
    "  optimizer:  momentum\n",
    "    learning_rate: 0.001\n",
    "    momentum amount: 0.9\n",
    "    minibatch size: 1\n",
    "layer 2:  fully connected\n",
    "  number of inputs: 17\n",
    "...\n",
    "```\n",
    "\n",
    "We can also see the performance history in a plot. This is helpful for getting a quick feel for how our model is performing.\n",
    "\n",
    "(If this image doesn't render, [you can view it here](https://gitlab.com/brohrer/study-norse-runes/blob/master/example_performance_history.png).)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![An example performance history on the Nordic Runes data set, showhing error decreasing over time.](example_performance_history.png \"Performance history\")\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To get a more detailed look at the model, including the nodes in each layer and the weights between them, we can browse through the snapshots generated while the model runs. This visualization gives a good pictorial representation of the model and helps to spark research questions and ideas for things to try in the next run.\n",
    "\n",
    "(If this image doesn't render, [you can view it here](https://gitlab.com/brohrer/study-norse-runes/blob/master/rune_training.gif).)\n",
    "\n",
    "![An animation of the autoencoder during training](rune_training.gif \"Autoencoder model during training\")\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    " # Next steps\n",
    " \n",
    "Now you’re in a good place. You have a fully functional\n",
    "autoencoder performing compression on an image dataset. You have a few options if you’d like to take it\n",
    "to the next level. You can experiment with different\n",
    "architectures. You can pull in a new set of images of your own.\n",
    "If you have been bitten by the bug, and really want to know\n",
    "how it all works, I have prepared series of courses for you\n",
    "at the\n",
    "[End-to-End Machine Learning School](https://end-to-end-machine-learning.teachable.com/courses):\n",
    "[193](https://end-to-end-machine-learning.teachable.com/p/how-deep-neural-networks-work/),\n",
    "[312](https://end-to-end-machine-learning.teachable.com/p/write-a-neural-network-framework),\n",
    "[313](https://end-to-end-machine-learning.teachable.com/p/advanced-neural-network-methods/), and\n",
    "[314](https://end-to-end-machine-learning.teachable.com/p/314-neural-network-optimization/).\n",
    "\n",
    "Or you can sign up for my\n",
    "Introduction to Neural Networks workshop sequence, offered\n",
    "at ODSC East 2020 in Boston covering\n",
    "[theory in part 1](https://odsc.com/speakers/introduction-to-deep-learning-neural-networks-i-concepts/) and\n",
    "[practice in part 2](https://odsc.com/speakers/introduction-to-deep-learning-neural-networks-ii-practice/)\n",
    "and at\n",
    "[SDSC 2020](https://www.southerndatascience.com/deeplearning20) in Atlanta."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(If this image doesn't render, [you can view it here](https://gitlab.com/brohrer/study-norse-runes/blob/master/example_nn_viz.png).)\n",
    "\n",
    "![A snapshot of the autoencoder during training](example_nn_viz.png \"Autoencoder model during training\")\n",
    "\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
